# Ejercicio para restaurar WP

En este repositorio solo hay una copia de la BD y una imagen que se utiliza en un post.
El ejercicio consite en crear un servidor Linux en blanco, no se pueden usar las imágenes de LEMP, LAMP o Wordpress.

Básicamente hay que instalar un serviddor web con PHP y MySQL o MariaDB.
Hay que instalar [Wordpress](https://wordpress.org/latest.zip) e importar la copia de la BD y las imágenes. 
Antes de importar la BD hay que editar el fichero testWP.sql buscar 46.183.115.141 y remplazarlo por la IP del servidor que hemos creado.

Datos de acceso al Admin de Wordpress:
- url: http://ip-servidor/wp-admin
- usuario: WPadmin
- contraseña: 5IVMdKcQmE0SOxZi

Se puede consultar cualquier artículo de nuestra base de conocimientos. Seguramente haga falta configurar algún tema en el administrador de Wordpress.

Se valorará cambiar la IP de Worpress por el nombre aleatorio de clouding.host y la generación de un SSL con Let's Encrypt. No se valorará el tiempo invertido en hacer el ejercicio, se valorará el resultado final. No borréis el history del servidor.

Podeís crear snapshot a medida que vayáis progresando en el ejercicio, así podréis retroceder a cualqueir paso en caso de fallo.

